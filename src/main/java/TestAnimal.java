/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Acer
 */
public class TestAnimal {
    public static void main(String[] args) {
        Human h1= new Human("Dang");
        h1.run();
        h1.eat();
        h1.walk();
        h1.speak();
        h1.sleep();
        System.out.println("---------------");
        Cat c1= new Cat("Pon");
        c1.run();
        c1.eat();
        c1.walk();
        c1.speak();
       c1.sleep();
        System.out.println("---------------");
        Dog d1= new Dog("Big");
        d1.run();
        d1.eat();
        d1.walk();
        d1.speak();
        d1.sleep();
        System.out.println("---------------");
        Crocodile cr1= new Crocodile("BU BU");
        cr1.crawl();
        cr1.eat();
        cr1.walk();
        cr1.speak();
        cr1.sleep();
        System.out.println("---------------");
        Snake s1= new Snake("Kheiyw");
        s1.crawl();
        s1.eat();
        s1.walk();
        s1.speak();
        s1.sleep();
        System.out.println("---------------");
        Angleworm aw1=new Angleworm("Ning");
        aw1.crawl();
        aw1.eat();
        aw1.walk();
        aw1.speak();
        aw1.sleep();
        System.out.println("---------------");
        Crab cb1= new Crab("Ploo");
        cb1.swim();
        cb1.eat();
        cb1.walk();
        cb1.speak();
        cb1.sleep();
        System.out.println("---------------");
        Fish f1=new Fish("Pla");
        f1.swim();
        f1.eat();
        f1.walk();
        f1.speak();
        f1.sleep();
        System.out.println("---------------");
        Bat b1=new Bat("Dom");
        b1.fly();
        b1.eat();
        b1.walk();
        b1.speak();
        b1.sleep();
        System.out.println("---------------");
        Bird br1=new Bird("Nok");
        br1.fly();
        br1.eat();
        br1.walk();
        br1.speak();
        br1.sleep();
        System.out.println("---------------");
        
        System.out.println("h1 is animal ? "+(h1 instanceof Animal));
        System.out.println("h1 is land animal ? "+(h1 instanceof LandAnimal));
        
        Animal a1=h1;
        System.out.println("a1 is land animal ? "+(a1 instanceof LandAnimal));
        System.out.println("a1 is reptile ? "+(a1 instanceof Reptile));
        
        System.out.println("c1 is animal ? "+(c1 instanceof Animal));
        System.out.println("c1 is land animal ? "+(c1 instanceof LandAnimal));
        System.out.println("d1 is animal ? "+(d1 instanceof Animal));
        System.out.println("d1 is land animal ? "+(d1 instanceof LandAnimal));
        System.out.println("cr1 is animal ? "+(cr1 instanceof Animal));
        Animal x=cr1;
        System.out.println("x is reptile ? "+(x instanceof Reptile));
        System.out.println("s1 is animal ? "+(s1 instanceof Animal));
        Animal x1=s1;
        System.out.println("x1 is reptile ? "+(x1 instanceof Reptile));
        System.out.println("aw1 is animal ? "+(aw1 instanceof Animal));
        Animal x2=aw1;
        System.out.println("x2 is reptile ? "+(x2 instanceof Reptile));
        System.out.println("cb1 is animal ? "+(cb1 instanceof Animal));
        Animal x3=cb1;
        System.out.println("x3 is reptile ? "+(x3 instanceof Reptile));
        System.out.println("s1 is animal ? "+(s1 instanceof Animal));
        Animal x4=s1;
        System.out.println("x4 is reptile ? "+(x4 instanceof AquaticAnimal));
        System.out.println("b1 is animal ? "+(b1 instanceof Animal));
        Animal x5=b1;
        System.out.println("x5 is reptile ? "+(x5 instanceof Reptile));
        System.out.println("br1 is animal ? "+(br1 instanceof Animal));
        Animal x6=br1;
        System.out.println("x is reptile ? "+(x6 instanceof Poultry));
    }
}
